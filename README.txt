NOTE:  The Gold edition does nothing except add a line that e-mails Agaric when you install with the profile.

This is a starter profile developed by (and originally for) Agaric Design Collective.
See http://agaricdesign.com/project/agaric_starter for more on this profile.

Ordinarily we'd never contribute quite such an ad-hoc thing to the Drupal downloads repository, but we feel we have to do what we can to make up for the lingering lack of a basic, normal profile for getting started a little bit faster than a default fresh Drupal install.  (The Wiki profile is great, but it involves patching modules.  Everything else as of 2007 June is language-specific or not meant for use yet.) 

That said, we want to use the crud project include file and do some things more like Drupal for Churches, which is an even better profile to model yours on for a more complex setup:
http://drupal.org/project/dfc

The Agaric Starter profile is based on Chapter 23 of Pro Drupal Development by Matt Westgate and John K. VanDyke and the default profile that comes with Drupal 5.


All non-core modules have been commented out.  If you would like to download and install them, here is a fairly random assortment of contributed modules we like.  Uncomment the lines as appropriate (or make new lines with only the modules you download and put in your modules/ or sites/all/modules folders ), be sure to include a comma after the last module in a line, as the below is really just one really long line as far as PHP is concerned.

    /* Contributed modules - divided by functionality then sorted alpha */
    
    // non-core bare necessities
//    'update_status', 
    
    // views
    'views', 'views_rss', 'views_theme_wizard', 'views_ui',
    
    // Agaric's essential picks
//    'activeselect', 'autosave', 'bookmark_us', 'event', 'event_all_day', 'event_views', 'image', 'img_assist', 'imagecache', 'logintoboggan', 'mimemail', 'panels', 'print', 'send', 'service_links', 
    
    // CCK - included in Agaric's essentials
//    'contemplate', 'content', 'content_copy', 'content_taxonomy', 'content_taxonomy_autocomplete', 'content_taxonomy_activeselect', 'content_taxonomy_options', 'content_taxonomy_views', 'email', 'fieldgroup', 'nodereference', 'number', 'optionwidgets', 'text', 
    
    // social network 
//    'invite', 'tagadelic', 'community_tags', 'form_store', 'mycap', 'quotes',
    
    // rating/voting
//    'fivestar', 'votingapi',
