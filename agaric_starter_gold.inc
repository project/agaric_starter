<?php

  // Agaric Starter Gold - adware and spyware

  variable_set('site_footer','<p align="center">Created and maintained by<br /><a href="/about-agaric-design-collective" class="agaric"><img src="http://agaricdesign.com/images/agariclogo.png" alt="Agaric Design" title="Agaric Design Collective"></a></p>');

  

  // Report by email that a new Drupal site has been installed.
  $to = 'support@agaricdesign.com';
  $from = ini_get('sendmail_from');
  $subject = st('New Agaric Gold Drupal site created!');
  $body = st('A new Agaric Starter Gold Installation Profile Drupal site was created: @site', array('@site' => base_path()));
  drupal_mail('agaric-starter-gold-profile', $to, $subject, $body, $from);

