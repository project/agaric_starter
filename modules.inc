<?php
/* there's probably something horribly unkosher about including snippets of code that aren't standalone functions in include files */

  return array(
    // Enable required core modules (we assume this line is NECESSARY)
    'block', 'filter', 'node', 'system', 'user', 'watchdog',
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
    // Enable optional core modules
    'aggregator', 'blog', 'comment', 'contact', 'forum', 'help', 'menu', 'path', 'search', 'statistics', 'taxonomy', 'throttle', 'tracker', 'upload', 
    
    /* Contributed modules - divided by functionality then sorted alpha */
    
    // non-core bare necessities
//    'update_status', 
    
    // views
    'views', 'views_rss', 'views_theme_wizard', 'views_ui',
    
    // Agaric's essential picks
//    'activeselect', 'autosave', 'bookmark_us', 'event', 'event_all_day', 'event_views', 'image', 'img_assist', 'imagecache', 'logintoboggan', 'mimemail', 'panels', 'print', 'send', 'service_links', 
    
    // CCK - included in Agaric's essentials
//    'contemplate', 'content', 'content_copy', 'content_taxonomy', 'content_taxonomy_autocomplete', 'content_taxonomy_activeselect', 'content_taxonomy_options', 'content_taxonomy_views', 'email', 'fieldgroup', 'imagefield', 'link', 'nodereference', 'number', 'optionwidgets', 'text', 
    
    // social network 
//    'invite', 'tagadelic', 'community_tags', 'form_store', 'mycap', 'quotes',
    
    // rating/voting
//    'fivestar', 'votingapi',
    );