<?php

See README.TXT

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *  An array with keys 'name' and 'description' describing the profile
 */
 
function agaric_starter_profile_details() {
  return array(
    'name' => st('Agaric Design Collective Starter Profile for Drupal'),
    'description' => st('Select this profile to get a quicker start on user roles and permissions, an about page, enabled modules, and more.')
  );
// st is used instead of t because when the installer runs, Drupal hasn't bootstrapped
}

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *  An array of moudles to be enabled.
 */
 
function agaric_starter_profile_modules() {
  include('modules.inc');
}

/**
 * Perform any final installation tasks for this profile.
 *
 * @return
 *   An optional HTML string to display to the user on the final installation
 *   screen.
 */
function agaric_starter_profile_final() {
  // Insert default user-defined node types into the database.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st('If you want to add a static page, like a contact page or an about page, use a page.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
    array(
      'type' => 'news',
      'name' => st('News item'),
      'module' => 'node',
      'description' => st('News itmes are simple articles: they have a title, a body (with a teaser created automatically or defined by text before <!--break-->).  News articles can be extended by other modules.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
  );

  foreach ($types as $type) {
    // use function from node.module 'Set default values for a node type defined through hook_node_info().'
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }

  // Default page to not be promoted, revision and have comments disabled.
  variable_set('node_options_page', array('status', 'revision'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);
  
  // Default news to be promoted and have revisions set, and have comments
  variable_set('node_options_news', array('status', 'revision', 'promote'));
  variable_set('comment_news', COMMENT_NODE_READ_WRITE);
  
  
  // Create a taxonomy for news
  $vocabulary = array(
    'name' => st('News Sections'),
    'description' => st('Select the appropriate category for your news item.'),
    'help' => st('You may select multiple sections.'),
    'nodes' => array('news' => st('News Item')),
    'hierarchy' => 0,
    'relations' => 0,
    'tags' => 0,
    'multiple' => 1,
    'required' => 0,
  );
  taxonomy_save_vocabulary($vocabulary);

// this is for an issue campaign site
  // Define some terms to categorize news items.
  $terms = array(
    st('Campaign updates'),
    st('Issue in the news'),
    st('Campaign in the news'),
    st('Inside the campaign'),
  );

  // Submit the "Add term" form programmatically for each term.
  foreach ($terms as $name) {
    drupal_execute('taxonomy_form_term', array('name' => $name), $vid);
  }

  // Add roles.
  db_query("INSERT INTO {role} (name) VALUES ('%s')", 'admin');


  // miscellaneous variabls
  
  variable_set('anonymous', 'Visitor');
  variable_set('site_frontpage', 'node');


  // this is so incredibly ill-advised it's beyond comprehension
  // ONLY uncomment this if you KNOW for certain clean URLs work on your server- you can enable it in admin/settings/clean-urls
  // see http://drupal.org/node/15365
//  variable_set('clean_url', 1);

}